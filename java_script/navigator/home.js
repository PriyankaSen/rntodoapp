import { createStackNavigator } from 'react-navigation';
import HomeScreen from '../screens/HomeScreen';
import DetailsScreen from '../screens/DetailsScreen';
import EditDetailsScreen from '../screens/EditDetails';

const HomeStack = createStackNavigator({
  Home: HomeScreen,
  Details: DetailsScreen,
  Edit: EditDetailsScreen,
},
  {
    initialRouteName: 'Home',
    /* The header config from Home Stack is now here */
    navigationOptions: {
      headerStyle: {
        backgroundColor: 'pink',
      },
      headerTintColor: 'black',
      headerTitleStyle: {
        fontWeight: 'bold',
      },
    },
  });

export default HomeStack;