import { createStackNavigator } from 'react-navigation';
import ProfileScreen from '../screens/ProfileScreen';
import EditProfile from '../screens/EditProfile';

const ProfileStack = createStackNavigator({
  Profile: ProfileScreen,
  EditProfile: EditProfile,
},
  {
    initialRouteName: 'Profile',
    /* The header config from Profile stack is now here */
    navigationOptions: {
      headerStyle: {
        backgroundColor: 'yellow',
      },
      headerTintColor: 'black',
      headerTitleStyle: {
        fontWeight: 'bold',
      },
    },
  });

export default ProfileStack;