import { createStackNavigator, createBottomTabNavigator } from 'react-navigation';
import LoginScreen from '../screens/LoginScreen';
import Registration from '../screens/Registration';
import ForgotPassword from '../screens/ForgotPassword';
import HomeStack from './home';
import ProfileStack from './profile';

export const LoginStack = createStackNavigator(
  {
    Login: LoginScreen,
    Registration: Registration,
    ForgotPassword: ForgotPassword,
  },
  {
    initialRouteName: 'Login',
    navigationOptions: {
      headerStyle: {
        backgroundColor: 'pink',
      },
      headerTintColor: '#fff',
      headerTitleStyle: {
        fontWeight: 'bold',
      },
    }
  }
);

export const MainTab = createBottomTabNavigator({
  Home: HomeStack,
  Profile: ProfileStack,
},{
  tabBarOptions: {
    activeTintColor: 'tomato',
    inactiveTintColor: 'gray',
  },
});