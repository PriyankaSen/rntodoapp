import React, { Component } from 'react';
import { StyleSheet, Text, View, Image} from 'react-native';
import { ButtonA } from '../components/Buttons';

class LogoTitle extends React.Component {
  render() {
    return (
      <View style={styles.headerViewStyle}>
        <Text style={styles.headerTitleStyle}>Edit Details</Text>
        <Image
          source={require('../../assets/success.png')}
          style={styles.headerImageStyle}
        />
      </View>

    );
  }
}


export default class EditProfileScreen extends Component {
  static navigationOptions = {
    // headerTitle instead of title
    headerTitle: <LogoTitle />,
  };

  render() {
    return (
      <View style={styles.container}>
        <Text style={styles.label}>Welcome to Edit Profile Screen!</Text>
        <ButtonA
          style={styles.button}
          onPress={() => this.props.navigation.navigate('Profile')}
          title="Profile"
        />
        <ButtonA
          style={styles.button}
          onPress={() => this.props.navigation.goBack()}
          title="Back"
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  headerViewStyle: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
  },
  headerImageStyle: {
    width: 20,
    height: 20
  },
  headerTitleStyle: {
    marginRight: 10,
  },
  label: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  button: {
    margin: 10,
    width: '40%'
  },
});
