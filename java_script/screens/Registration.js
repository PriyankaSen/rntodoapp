import React, { Component } from 'react';
import { StyleSheet, Text, View, TextInput } from 'react-native';
import { ButtonA, ButtonB } from '../components/Buttons';

export default class Registration extends Component {

  constructor(props) {
    super(props);
    this.state = {
      email: '',
      password: ''
    };
  }

  login = () => {

  }
  render() {
    return (
      <View style={styles.container}>
        <View style={styles.inputView}>
          <Text style={styles.label}>Email:</Text>
          <TextInput
           style={styles.input}
           onChangeText={(email) => this.setState({email})}
           value={this.state.email}
          />
        </View>
        <View style={styles.inputView}>
        <Text style={styles.label}>Password:</Text>
        <TextInput
          style={styles.input}
          onChangeText={(password) => this.setState({password})}
          value={this.state.password}
        />
        </View>
        <View style={styles.inputView}>
        <Text style={styles.label}>Confirm:</Text>
        <TextInput
          style={styles.input}
          onChangeText={(password) => this.setState({password})}
          value={this.state.password}
        />
        </View>
        <View style={styles.actionView}>
        <ButtonA
          style={styles.button}
          onPress={() => this.props.navigation.navigate('Registration')}
          title="Registration"
        />
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  label: {
    fontSize: 20,
    textAlign: 'left',
    margin: 10,
    width: 100,
  },
  button: {
    margin: 10,
    width: '40%'
  },
  input: {
    height: 40,
    borderColor: 'gray',
    borderWidth: 1,
    flex: 1,
    borderRadius: 5,
    marginRight: 15,
  },
  inputView: {
    flexDirection: 'row',
    height: 40,
    margin: 10,
    width: '95%',
    alignSelf: 'center',
  },
  actionView: {
    flexDirection: 'row',
    margin: 10,
    width: '95%',
    alignSelf: 'center',
    justifyContent: 'center',
  },
});
