import React, { Component } from 'react';
import { StyleSheet, Text, View, TextInput, AsyncStorage } from 'react-native';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { auth } from '../redux/actions';
import { ButtonA, ButtonB } from '../components/Buttons';

class LoginScreen extends Component {

  constructor(props) {
    super(props);
    this.state = {
      email: '',
      password: ''
    };
  }

  _storeData = async () => {
    const form = {'email': this.state.email, 'password': this.state.password, isLoggedIn: true };
    try {
      await AsyncStorage.setItem('creds', JSON.stringify(form));
      //alert('Saved: ' + JSON.stringify(form));
      this._retrieveData();
    } catch (error) {
      // Error saving data
      alert(JSON.stringify(error));
    }
  }

  _retrieveData = async () => {
    try {
      const value = await AsyncStorage.getItem('creds');
      alert('data: ' + value);
      if (value) {
        // We have data!!
        
        // this.setState({
        //     isLoggedIn: value.isLoggedIn,
        // });
      }
     } catch (error) {
       // Error retrieving data
       alert('Error: ' + JSON.stringify(error));
     }
  }

  login = () => {
    this._storeData();//need to update global state through redux to show main tabs
  }

  render() {
    return (
      <View style={styles.container}>
        <View style={styles.inputView}>
          <Text style={styles.label}>Email:</Text>
          <TextInput
           style={styles.input}
           onChangeText={(email) => this.setState({email})}
           value={this.state.email}
          />
        </View>
        <View style={styles.inputView}>
        <Text style={styles.label}>Password:</Text>
        <TextInput
          style={styles.input}
          onChangeText={(password) => this.setState({password})}
          value={this.state.password}
        />
        </View>
        <View style={styles.actionView}>
        <ButtonA
          style={styles.button}
          onPress={this.login}
          title="Login"
        />
        <ButtonA
          style={styles.button}
          onPress={() => this.props.navigation.navigate('Registration')}
          title="New User"
        />
        </View>
        <ButtonB
          style={styles.button}
          onPress={() => this.props.navigation.navigate('ForgotPassword')}
          title="Forgot Password"
        />
      </View>
    );
  }
}

function mapStateToProps(state) {
  return {
      auth: state.isLoggedIn
  }
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(auth, dispatch);
}

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(LoginScreen)

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  label: {
    fontSize: 20,
    textAlign: 'left',
    marginHorizontal: 10,
    width: 100,
  },
  button: {
    margin: 10,
    width: '40%'
  },
  input: {
    height: 40,
    borderColor: 'gray',
    borderWidth: 1,
    flex: 1,
    borderRadius: 5,
    marginRight: 15,
  },
  inputView: {
    flexDirection: 'row',
    height: 40,
    margin: 10,
    width: '95%',
    alignSelf: 'center',
  },
  actionView: {
    flexDirection: 'row',
    margin: 10,
    width: '95%',
    alignSelf: 'center',
    justifyContent: 'center',
  },
});
