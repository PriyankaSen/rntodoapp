import React, { Component } from 'react';
import { StyleSheet, Text, View} from 'react-native';
import { ButtonA, ButtonB } from '../components/Buttons';

export default class ProfileScreen extends Component {
  static navigationOptions = {
    title: 'Prifile',
    /*Adding a button to the header*/
    headerRight: (
      <ButtonB
        onPress={() => alert('This is a button!')}
        title="Info"
        color="#fff"
      />
    ),
  };
  render() {
    return (
      <View style={styles.container}>
        <Text style={styles.label}>Welcome to Profile Screen!</Text>
        <ButtonA
          style={styles.button}
          onPress={() => this.props.navigation.navigate('EditProfile')}
          title="Edit"
        />
        <ButtonB
          style={styles.button}
          onPress={() => this.props.navigation.navigate('Home')}
          title="Home"
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  label: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  button: {
    margin: 10,
    width: '40%'
  },
});