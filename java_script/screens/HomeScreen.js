import React, { Component } from 'react';
import { StyleSheet, Text, View} from 'react-native';
import { ButtonA, ButtonB } from '../components/Buttons';

export default class HomeScreen extends Component {
  static navigationOptions = {
    title: 'Home',
    headerTitleStyle: {
      fontWeight: 'bold',
    },
  };
  render() {
    return (
      <View style={styles.container}>
        <Text style={styles.label}>Welcome to Home Screen!</Text>
        <ButtonA
          style={styles.button}
          onPress={() => this.props.navigation.navigate('Details',{
            itemId: 86,
            otherParam: 'This is a param from home screen',
            title: 'Dynamic Title',
          })}
          title="Details"
        />
        <ButtonB
          style={styles.button}
          onPress={() => this.props.navigation.navigate('EditProfile')}
          title="Profile"
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  label: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  button: {
    margin: 10,
    width: '40%'
  },
});
