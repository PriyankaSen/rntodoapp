import React, { Component } from 'react';
import { StyleSheet, Text, View} from 'react-native';
import { ButtonA } from '../components/Buttons';

export default class DetailsScreen extends Component {
  static navigationOptions = ({ navigation }) => {
    return {
      title: navigation.getParam('title', 'Details'),
    };
  };
  render() {
    const { navigation } = this.props;
    const itemId = navigation.getParam('itemId', 'NO-ID');
    const otherParam = navigation.getParam('otherParam', 'some default value');
    return (
      <View style={styles.container}>
        <Text style={styles.label}>Welcome to Details Screen! {'\n' + itemId} {'\n' + otherParam}</Text>
        <View style={styles.subView}>
          <ButtonA
            style={styles.button}
            onPress={() =>
              this.props.navigation.push('Details', {
                itemId: Math.floor(Math.random() * 100),
              })}
            title="Again Details"
          />
          <ButtonA
            style={styles.button}
            onPress={() => this.props.navigation.navigate('Details')}
            title="Details"
          />
        </View>
        <View style={styles.subView}>
          <ButtonA
            style={styles.button}
            onPress={() => this.props.navigation.goBack()}
            title="Back"
          />
          <ButtonA
            style={styles.button}
            onPress={() => this.props.navigation.navigate('Home')}
            title="Home"
          />
        </View>
        <View style={styles.subView}>
          <ButtonA
            style={styles.button}
            onPress={() => this.props.navigation.setParams({title: 'Updated Title!'})}
            title="Update Title"
          />
          <ButtonA
            style={styles.button}
            onPress={() => this.props.navigation.push('Edit')}
            title="Edit Details"
          />
        </View>



      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  subView: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
    flexDirection: 'row'
  },
  label: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  button: {
    margin: 10,
    width: '40%'
  },
});
