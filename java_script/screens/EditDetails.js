import React, { Component } from 'react';
import { StyleSheet, Text, View} from 'react-native';
import { ButtonA } from '../components/Buttons';

export default class EditDetailsScreen extends Component {
  static navigationOptions = ({ navigationOptions }) => {
    return {
      title: 'Edit Details',
      /*Overriding shared navigationOptions*/
      headerStyle: {
        backgroundColor: navigationOptions.headerTintColor,
      },
      headerTintColor: navigationOptions.headerStyle.backgroundColor,
      headerTitleStyle: {
        fontWeight: 'bold',
      },
    }
  };
  render() {
    const { navigation } = this.props;
    const itemId = navigation.getParam('itemId', 'NO-ID');
    const otherParam = navigation.getParam('otherParam', 'some default value');
    return (
      <View style={styles.container}>
        <Text style={styles.label}>Welcome to Edit Details Screen! {'\n' + itemId} {'\n' + otherParam}</Text>
        <ButtonA
          style={styles.button}
          onPress={() => this.props.navigation.goBack()}
          title="Back"
        />
        <ButtonA
          style={styles.button}
          onPress={() => this.props.navigation.navigate('Home')}
          title="Home"
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  label: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  button: {
    margin: 10,
    width: '40%'
  },
});
