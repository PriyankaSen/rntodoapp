import React from 'react';
import { StyleSheet, View, Text, Image, TouchableHighlight } from 'react-native';

export class ButtonA extends React.Component {

    onClick = () => {
        alert('hey!!');
    }
    render() {
        return (
            <TouchableHighlight
                style={this.props.style}
                underlayColor='transparent'
                onPress={this.props.onPress}
            >
                <View style={styles.solid}>
                    <Text style={styles.title}>{this.props.title}</Text>
                </View>
            </TouchableHighlight>
        );
    }
}

export class ButtonB extends React.Component {

    onClick = () => {
        alert('hey!!');
    }
    render() {
        return (
            <TouchableHighlight
                style={this.props.style}
                underlayColor='transparent'
                onPress={this.props.onPress}
            >
                <View style={styles.clear}>
                    <Text style={styles.linkTitle}>{this.props.title}</Text>
                </View>
            </TouchableHighlight>
        );
    }
}

const styles = StyleSheet.create({
    solid: {
      justifyContent: 'center',
      alignItems: 'center',
      backgroundColor: 'blue',
      borderRadius: 5,
      
    },
    clear: {
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'transparent',
        
    },
    title: {
      fontSize: 20,
      textAlign: 'center',
      color: 'white',
      margin: 10,
    },
    linkTitle: {
        fontSize: 20,
        textAlign: 'center',
        color: 'blue',
        margin: 10,
        textDecorationLine: 'underline',
    },
  });