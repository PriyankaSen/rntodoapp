import { applyMiddleware, createStore } from 'redux'
import rootReducer from './reducers';
import thunk from 'redux-thunk';
import {persistStore} from 'redux-persist';

const middlewares = [thunk];

if (process.env.NODE_ENV === 'development') {
  const { logger } = require('redux-logger');
  middlewares.push(logger);
}

const store = createStore(rootReducer, applyMiddleware(...middlewares));

export const persistor = persistStore(store);
export default store;