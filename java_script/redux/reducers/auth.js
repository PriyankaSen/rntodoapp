import { UPDATE_LOGIN_STATUS } from '../actions/auth';

export const initialState = {
    isLoggedIn: false,
}

const auth = (state = initialState, action) => {
    switch (action.type) {
        case UPDATE_LOGIN_STATUS:
            return { ...state, isLoggedIn: action.payload };
        default:
            return state;
    }
}

export default auth;