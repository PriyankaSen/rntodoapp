export const UPDATE_LOGIN_STATUS = 'UPDATE_LOGIN_STATUS';

export const setLoginStatus = payload => {
    return {
        type: UPDATE_LOGIN_STATUS,
        payload
    }

}