import React from 'react';
import { AsyncStorage } from 'react-native';
import { Provider, connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { auth } from '../redux/actions';

import { LoginStack ,MainTab } from './java_script/navigator';

class App extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            isLoggedIn: this.props.isLoggedIn,
        }
        this._retrieveData();
    }
    _retrieveData = async () => {
        try {
          const value = await AsyncStorage.getItem('creds');
          if (value !== null) {
            const creds = JSON.parse(value);
            this.setState({
                isLoggedIn: creds.isLoggedIn,
            });
          }
         } catch (error) {
           // Error retrieving data
           alert('Error: ' + JSON.stringify(error));
         }
    }
    render() {
        return (
            <Provider store={store}>
            {this.props.isLoggedIn?<MainTab/>:<LoginStack/>}
            </Provider>
            
        )
    }
}

const mapStateToProps = (state) => ({
    isLoggedIn: state.isLoggedIn
});

const mapDispatchToProps = (dispatch) => ({
    updateLoginStatus: dispatch(updateLoginStatus),
});

export default connect(
    mapStateToProps,
    mapDispatchToProps,
)(App)